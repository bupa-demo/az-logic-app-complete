#virtual network
module "vnet" {
  source                = "git::https://gitlab.com/bupa-demo/az-virtual-network.git"
  vnet_name             = var.vnet_name
  resource_group_name   = module.logic_app_rg.resource_group_name
  location              = module.logic_app_rg.resource_group_location
  vnet_cidr             = var.vnet_cidr
  dns_servers           = var.dns_servers
  tags                  = var.default_tags
}

#route table
module "logic_app_route_table" {
  source                        = "git::https://gitlab.com/bupa-demo/az-route-table.git"
  route_table_name              = var.logic_app_route_table_name
  location                      = module.logic_app_rg.resource_group_location
  resource_group_name           = module.logic_app_rg.resource_group_name
  disable_bgp_route_propagation = var.disable_bgp_route_propagation
  route_name                    = var.logic_app_route_name
  enable_force_tunneling        = var.enable_force_tunneling
  tags                          = var.default_tags
}

#network security group
module "logic_app_nsg" {
  source                        = "git::https://gitlab.com/bupa-demo/az-network-security-group.git"
  network_security_group_name   = var.logic_app_network_security_group_name
  resource_group_name           = module.logic_app_rg.resource_group_name
  location                      = module.logic_app_rg.resource_group_location
  ingress_rules                 = var.ingress_rules
  egress_rules                  = var.egress_rules
  tags                          = var.default_tags
}

#subnet
module "logic_app_subnet" {
  source                    = "git::https://gitlab.com/bupa-demo/az-subnet.git"
  subnet_name               = var.logic_app_subnet_name
  resource_group_name       = module.logic_app_rg.resource_group_name
  virtual_network_name      = module.vnet.virtual_network_name
  subnet_cidr_list          = var.logic_app_subnet_cidr_list
  network_security_group_id = module.logic_app_nsg.network_security_group_id
  route_table_id            = module.logic_app_route_table.route_table_id
}