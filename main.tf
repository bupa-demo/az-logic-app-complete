#resource group
module "logic_app_rg" {
  source          = "git::https://gitlab.com/bupa-demo/az-resource-group.git"
  rg_name         = var.resource_group_name
  location        = var.location
  tags            = var.default_tags
}

#app service plan
module "app_service_plan" {
  source                       = "git::https://gitlab.com/bupa-demo/az-app-service-plan.git"
  app_service_plan_name        = var.app_service_plan_name  
  location                     = module.logic_app_rg.resource_group_location  
  resource_group_name          = module.logic_app_rg.resource_group_name  
  os_type                      = var.os_type  
  sku_name                     = var.sku_name  
  worker_count                 = var.sku_name == "Y1" ? null : var.worker_count  
  maximum_elastic_worker_count = var.maximum_elastic_worker_count  
  app_service_environment_id   = var.app_service_environment_id  
  per_site_scaling_enabled     = var.per_site_scaling_enabled  
  default_tags                 = var.default_tags
}

#storage account
module "storage_account" {
  source                              = "git::https://gitlab.com/bupa-demo/az-storage-account.git"
  resource_group_name                 = module.logic_app_rg.resource_group_name  
  location                            = module.logic_app_rg.resource_group_location
  storage_name                        = var.storage_name
  replication_type                    = var.replication_type
  account_kind                        = var.account_kind
  account_tier                        = var.account_tier
  access_tier                         = var.access_tier
  enable_large_file_share             = var.enable_large_file_share
  enable_hns                          = var.enable_hns
  enable_https_traffic_only           = var.enable_https_traffic_only
  min_tls_version                     = var.min_tls_version
  allow_nested_items_to_be_public     = var.allow_nested_items_to_be_public
  access_list                         = var.access_list
  service_endpoints                   = var.service_endpoints
  traffic_bypass                      = var.traffic_bypass
  blob_delete_retention_days          = var.blob_delete_retention_days
  blob_cors                           = var.blob_cors
  encryption_scopes                   = var.encryption_scopes
  infrastructure_encryption_enabled   = var.infrastructure_encryption_enabled
  nfsv3_enabled                       = var.nfsv3_enabled
  default_network_rule                = var.default_network_rule
  shared_access_key_enabled           = var.shared_access_key_enabled
  blob_versioning_enabled             = var.blob_versioning_enabled
  container_delete_retention_days     = var.container_delete_retention_days
  tags                                = var.default_tags
}

#private endpoint
module "private_endpoint" {
  source                              = "git::https://gitlab.com/bupa-demo/az-private-endpoint.git"

  pe_resource_group_name              = module.logic_app_rg.resource_group_name
  location                            = module.logic_app_rg.resource_group_location
  private_endpoint_name               = var.private_endpoint_name
  pe_subnet_id                        = module.logic_app_subnet.subnet_id
  dns                                 = {
                                        zone_ids  = [azurerm_private_dns_zone.dns-zone.id]
                                        zone_name = azurerm_private_dns_zone.dns-zone.name
                                        }
  endpoint_resource_id                = module.storage_account.storage_account_id
  subresource_names                   = var.subresource_names
  is_manual_connection                = var.is_manual_connection
  tags                                = var.default_tags
}

resource "azurerm_private_dns_zone" "dns-zone" {
  name                = var.private_dns_zone
  resource_group_name = module.logic_app_rg.resource_group_name
}

resource "azurerm_private_dns_zone_virtual_network_link" "network_link" {
  name                  = var.private_endpoint_name_vnl
  resource_group_name   = module.logic_app_rg.resource_group_name
  private_dns_zone_name = azurerm_private_dns_zone.dns-zone.name
  virtual_network_id    = module.vnet.virtual_network_id
}