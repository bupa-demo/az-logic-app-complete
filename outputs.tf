# Resource Group
output "resource_group_name" {
  value       = module.logic_app_rg.resource_group_name
  description = "Resource group name"
}

output "resource_group_id" {
  value       = module.logic_app_rg.resource_group_id
  description = "Resource group id"
}

output "resource_group_location" {
  value       = module.logic_app_rg.resource_group_location
  description = "Resource group location"
}

# App Service plan
output "service_plan_id" {
  description = "ID of the created Service Plan"
  value       = module.app_service_plan.service_plan_id
}

output "service_plan_name" {
  description = "Name of the created Service Plan"
  value       = module.app_service_plan.service_plan_name
}

output "service_plan_location" {
  description = "Azure location of the created Service Plan"
  value       = module.app_service_plan.service_plan_location
}

# vNET
output "virtual_network_id" {
  description = "Virtual network generated id"
  value       = module.vnet.virtual_network_id
}

output "virtual_network_name" {
  description = "Virtual network name"
  value       = module.vnet.virtual_network_name
}

output "virtual_network_space" {
  description = "Virtual network space"
  value       = module.vnet.virtual_network_space
}


# Subnet
output "subnet_id" {
  description = "Id of the created subnet"
  value       = module.logic_app_subnet.subnet_id
}

output "subnet_cidr_list" {
  description = "CIDR list of the created subnets"
  value       = module.logic_app_subnet.subnet_cidr_list
}

output "subnet_names" {
  description = "Names of the created subnet"
  value       = module.logic_app_subnet.subnet_names
}

# Routes
output "route_table_name" {
  description = "Route table name"
  value       = module.logic_app_route_table.route_table_name
}

output "route_table_id" {
  description = "Route table ID"
  value       = module.logic_app_route_table.route_table_id
}

output "route_force_tunneling" {
  description = "Force tunneling route status"
  value       = var.enable_force_tunneling
}

# NSG
output "network_security_group_id" {
  description = "Network security group id"
  value       = module.logic_app_nsg.network_security_group_id
}

output "network_security_group_name" {
  description = "Network security group name"
  value       = module.logic_app_nsg.network_security_group_name
}

# Private Endpoint
output "private_endpoint_name" {
  value = module.private_endpoint.private_endpoint_name
}

output "private_endpoint_id" {
  value = module.private_endpoint.private_endpoint_id
}

# Storage
output "storage_account_name" {
  value       = module.storage_account.storage_account_name
  description = "The name of the Storage Account."
}

output "storage_account_id" {
  value       = module.storage_account.storage_account_id
  description = "The ID of the Storage Account."
}
